//! Hardpoints module

use crate::modules::Class;
use std::fmt;

/// An enum for all hardpoints modules
pub enum HardpointModule {
    /// A pulse laser
    PulseLaser(PulseLaser),
}

impl HardpointModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            HardpointModule::PulseLaser(pulse_laser) => pulse_laser.class,
        }
    }
}

impl fmt::Debug for HardpointModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "H(")?;
        match self {
            HardpointModule::PulseLaser(pulse_laser) => {
                write!(f, "{:?}", pulse_laser)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit a hardpoint module
#[derive(Debug)]
pub struct HardpointSlot {
    /// The class of the slot, only hardpoint modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<HardpointModule>,
}

/// The pulse laser hardpoint
#[derive(Debug)]
pub struct PulseLaser {
    /// The class of the module
    pub class: Class,
}
