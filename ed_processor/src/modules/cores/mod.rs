//! Core modules

use crate::modules::cores::fsd::FSD;
use crate::modules::Class;
use crate::Tons;
use std::fmt;

pub mod fsd;

/// An enum for all the core modules
pub enum CoreModule {
    /// A module to be mounted in a ship's armor slot
    Armor(Armor),
    /// A module to be mounted in a ship's power plant slot
    PowerPlant(PowerPlantModule),
    /// A module that to be mounted in a ship's thrusters slot
    Thrusters(ThrustersModule),
    /// A module to be mounted in a ship's FSD slot
    FSD(FSD),
    /// A module to be mounted in a ship's life support slot
    LifeSupport(LifeSupport),
    /// A module to be mounted in a ship's power distributor slot
    PowerDistributor(PowerDistributorModule),
    /// A module to be mounted in a ship's sensors slot
    Sensors(Sensors),
    /// A module to be mounted in a ship's fuel tank slot
    FuelTank(FuelTank),
}

impl CoreModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            CoreModule::Armor(armor) => armor.class(),
            CoreModule::PowerPlant(power_plant) => power_plant.class(),
            CoreModule::Thrusters(thrusters) => thrusters.class(),
            CoreModule::FSD(fsd) => fsd.class,
            CoreModule::LifeSupport(life_support) => life_support.class,
            CoreModule::PowerDistributor(power_distributor) => power_distributor.class(),
            CoreModule::Sensors(sensors) => sensors.class,
            CoreModule::FuelTank(fuel_tank) => fuel_tank.class,
        }
    }
}

impl fmt::Debug for CoreModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "C(")?;
        match self {
            CoreModule::Armor(armor) => write!(f, "{:?}", armor),
            CoreModule::PowerPlant(power_plant) => write!(f, "{:?}", power_plant),
            CoreModule::Thrusters(thrusters) => write!(f, "{:?}", thrusters),
            CoreModule::FSD(fsd) => write!(f, "{:?}", fsd),
            CoreModule::LifeSupport(life_support) => write!(f, "{:?}", life_support),
            CoreModule::PowerDistributor(power_distributor) => write!(f, "{:?}", power_distributor),
            CoreModule::Sensors(sensors) => write!(f, "{:?}", sensors),
            CoreModule::FuelTank(fuel_tank) => write!(f, "{:?}", fuel_tank),
        }?;
        write!(f, ")")
    }
}

/// The Armor module
#[derive(Debug)]
pub enum Armor {
    /// The lightweight armor
    Lightweight,
    /// The reinforced armor
    Reinforced,
    /// The military grade armor
    Military,
    /// the mirrored armor
    Mirrored,
    /// The reactive armor
    Reactive,
}

impl Armor {
    /// Get the class of the module
    pub fn class(&self) -> Class {
        class!(1)
    }
}

/// A slot that can fit an armor module
#[derive(Debug)]
pub struct ArmorSlot {
    /// The class of the slot, only armor modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<Armor>,
}

impl ArmorSlot {
    /// Get a new armor slot, fitted with the stock module
    pub fn stock() -> Self {
        ArmorSlot {
            class: class!(1),
            module: Some(Armor::Lightweight),
        }
    }
}

/// The Fuel Tank module
#[derive(Debug)]
pub struct FuelTank {
    /// The fuel content of the module
    pub content: Tons,
    /// The class of the module
    pub class: Class,
}

/// A slot that can fit a fuel tank module
#[derive(Debug)]
pub struct FuelTankSlot {
    /// The class of the slot, only fuel tank modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<FuelTank>,
}

impl FuelTankSlot {
    /// Get a new fuel tank slot, fitted with the stock module
    pub fn stock(class: Class) -> Self {
        FuelTankSlot {
            class,
            module: Some(FuelTank {
                class,
                content: 2_f64.powi(class as i32).into(),
            }),
        }
    }
}

/// The Life Support module
#[derive(Debug)]
pub struct LifeSupport {
    /// The class of the module
    pub class: Class,
}

/// A slot that can fit a life support module
#[derive(Debug)]
pub struct LifeSupportSlot {
    /// The class of the slot, only life support modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<LifeSupport>,
}

impl LifeSupportSlot {
    /// Get a new life support slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        LifeSupportSlot {
            class,
            module: Some(LifeSupport { class }),
        }
    }
}

/// An enum for the different kinds of power distributors
pub enum PowerDistributorModule {
    /// A power distributor made from human technology only
    PowerDistributor(PowerDistributor),
    /// A power distributor made from both human and guardian technology
    GuardianPowerDistributor(GuardianPowerDistributor),
}

impl PowerDistributorModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            PowerDistributorModule::PowerDistributor(power_distributor) => power_distributor.class,
            PowerDistributorModule::GuardianPowerDistributor(power_distributor) => {
                power_distributor.class
            }
        }
    }
}

impl fmt::Debug for PowerDistributorModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PD(")?;
        match self {
            PowerDistributorModule::PowerDistributor(power_distributor) => {
                write!(f, "{:?}", power_distributor)
            }
            PowerDistributorModule::GuardianPowerDistributor(guardian_power_distributor) => {
                write!(f, "{:?}", guardian_power_distributor)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit a power distributor module
#[derive(Debug)]
pub struct PowerDistributorSlot {
    /// The class of the slot, only modules fo class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<PowerDistributorModule>,
}
impl PowerDistributorSlot {
    /// Get a new power distributor slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        PowerDistributorSlot {
            class,
            module: Some(PowerDistributorModule::PowerDistributor(PowerDistributor {
                class,
            })),
        }
    }
}

/// The guardian power distributor module
#[derive(Debug)]
pub struct GuardianPowerDistributor {
    /// The class of the module
    pub class: Class,
}

/// The power distributor module
#[derive(Debug)]
pub struct PowerDistributor {
    /// The class of the module
    pub class: Class,
}

/// An enum for the different kinds of power plant
pub enum PowerPlantModule {
    /// A power plant made from human technology only
    PowerPlant(PowerPlant),
    /// A power plant made from both human and guardian technology
    GuardianPowerPlant(GuardianPowerPlant),
}

impl PowerPlantModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            PowerPlantModule::PowerPlant(power_plant) => power_plant.class,
            PowerPlantModule::GuardianPowerPlant(power_plant) => power_plant.class,
        }
    }
}

impl fmt::Debug for PowerPlantModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PP(")?;
        match self {
            PowerPlantModule::PowerPlant(power_plant) => write!(f, "{:?}", power_plant),
            PowerPlantModule::GuardianPowerPlant(guardian_power_plant) => {
                write!(f, "{:?}", guardian_power_plant)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit a power plant module
#[derive(Debug)]
pub struct PowerPlantSlot {
    /// The class of the slot, only modules fo class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<PowerPlantModule>,
}

impl PowerPlantSlot {
    /// Get a new power plant slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        PowerPlantSlot {
            class,
            module: Some(PowerPlantModule::PowerPlant(PowerPlant { class })),
        }
    }
}

/// The guardian power plant module
#[derive(Debug)]
pub struct GuardianPowerPlant {
    /// The class of the module
    pub class: Class,
}

/// The power plant module
#[derive(Debug)]
pub struct PowerPlant {
    /// The class of the module
    pub class: Class,
}

/// The Sensors module
#[derive(Debug)]
pub struct Sensors {
    /// The class of the module
    pub class: Class,
}

/// A slot that can fit a sensors module
#[derive(Debug)]
pub struct SensorsSlot {
    /// The class of the slot, only sensors modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<Sensors>,
}

impl SensorsSlot {
    /// Get a new sensors slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        SensorsSlot {
            class,
            module: Some(Sensors { class }),
        }
    }
}

/// An enum for the different kind of thrusters
pub enum ThrustersModule {
    /// Standard thrusters
    Thruster(Thrusters),
    /// Thrusters enhanced by engineers
    EnhancedThruster(EnhancedThrusters),
}

impl ThrustersModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            ThrustersModule::Thruster(thrusters) => thrusters.class,
            ThrustersModule::EnhancedThruster(thrusters) => thrusters.class,
        }
    }
}

impl fmt::Debug for ThrustersModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "T(")?;
        match self {
            ThrustersModule::Thruster(thrusters) => write!(f, "{:?}", thrusters),
            ThrustersModule::EnhancedThruster(enhanced_thrusters) => {
                write!(f, "{:?}", enhanced_thrusters)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit a thrusters module
#[derive(Debug)]
pub struct ThrustersSlot {
    /// The class of the slot, only modules fo class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<ThrustersModule>,
}

impl ThrustersSlot {
    /// Get a new thrusters slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        ThrustersSlot {
            class,
            module: Some(ThrustersModule::Thruster(Thrusters { class })),
        }
    }
}

/// The enhanced thrusters module
#[derive(Debug)]
pub struct EnhancedThrusters {
    /// The class of the module
    pub class: Class,
}

/// The thrusters module is what propels a ship and allow it to move in normal space.
#[derive(Debug)]
pub struct Thrusters {
    /// The class of the module
    pub class: Class,
}
